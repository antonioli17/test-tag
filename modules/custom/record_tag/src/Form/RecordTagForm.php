<?php

namespace Drupal\record_tag\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\node\Entity\Node;
use Drupal;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Egulias\EmailValidator\EmailValidator;

/**
 * Class RecordTagForm.
 */
class RecordTagForm extends FormBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The term Storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * The email validator.
   *
   * @var \Egulias\EmailValidator\EmailValidator
   */
  protected $emailValidator;

  /**
   * ResourceController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * 
   * @param \Egulias\EmailValidator\EmailValidator $email_validator
   *   The email validator.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EmailValidator $email_validator) {
    $this->entityTypeManager = $entity_type_manager;
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
    $this->emailValidator = $email_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('email.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'record_tag_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $terms = $this->termStorage->loadTree('class_subject');
    $class_subject = [];
    if (!empty($terms)) {
      foreach($terms as $term) {
        $class_subject[$term->tid] = $this->t($term->name);
      }
    }

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username (Only Alphanumeric Characters)'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#required' => 'true',
      '#pattern' => '^[a-zA-Z0-9]{1,50}$',
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#weight' => '0',
      '#default_value' => isset($result['email']) ? $result['email'] : '',
      '#required' => 'true',
    ];

    $form['class_subject'] = [
      '#type' => 'select',
      '#title' => $this->t('Class Subject'),
      '#validated' => 'true',
      '#options' => $class_subject,
      '#size' => 1,
      '#weight' => '0',
      '#required' => 'true',
      '#ajax'         => [
        'callback'  => '::topicCallback',
        'wrapper'   => 'topic-wrapper',
      ],
    ];

    $form['select_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'topic-wrapper'],
    ];

    $form['select_wrapper']['class_topic'] = [
      '#type' => 'select',
      '#title' => $this->t('Class Topic'),
      '#validated' => 'true',
      '#options' => [],
      '#size' => 1,
      '#weight' => '0',
      '#required' => 'true',
      '#ajax'         => [
        'callback'  => '::timeLotCallback',
        'wrapper'   => 'time-wrapper',
      ],
    ];

    $form['select_wrapper']['select_wrapper_time'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'time-wrapper'],
    ];

    $form['select_wrapper']['select_wrapper_time']['class_timeslot'] = [
      '#type' => 'select',
      '#title' => $this->t('Class Timeslot'),
      '#validated' => 'true',
      '#options' => [],
      '#size' => 1,
      '#weight' => '0',
      '#required' => 'true',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function topicCallback(array $form, FormStateInterface $form_state) {
    $class_subject = $form_state->getValue('class_subject');

    $options = [];
    foreach ($this->termStorage->load($class_subject)->get('field_class_topic')->getValue() as $topic) {
      $paragraph = $this->entityTypeManager->getStorage('paragraph')->load($topic['target_id']);
      $options[$topic['target_id']] = $paragraph->get('field_class_topic')->getValue()[0]['value'];
    }

    $form['select_wrapper']['class_topic']['#options'] = $options;
    $form_state->setRebuild();   

    return $form['select_wrapper'];
  }


  /**
   * {@inheritdoc}
   */
  public function timeLotCallback(array $form, FormStateInterface $form_state) {
    $class_topic = $form_state->getValue('class_topic');

    $paragraph = $this->entityTypeManager->getStorage('paragraph')->load($class_topic);

    $options = [];
    foreach ($paragraph->get('field_class_timeslot')->getValue() as $row) {
      $options[$row['value']] = $row['value'];
    }

    $form['select_wrapper']['select_wrapper_time']['class_timeslot']['#options'] = $options;

    $form_state->setRebuild();

    return $form['select_wrapper']['select_wrapper_time'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $email = trim($form_state->getValue('email')); 
    if (!$this->emailValidator->isValid($email)) { 
        $form_state->setErrorByName('email', $this->t('%email is an invalid email address.', array('%email' => $email))); 
    }

    foreach ($form_state->getValues() as $key => $value) {
      if (empty($value)) {
          $form_state->setErrorByName($key, str_replace('_', ' ', ucfirst($key)) . ' ' . $this->t('is required.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $node = $this->entityTypeManager->getStorage('node')->create([
      'type' => 'tag',
      'title' => $values['username'],
      'field_email' => $values['email'],
      'field_class_subject' => taxonomy_term_load($values['class_subject'])->get('name')->value,
      'field_class_topic' => $this->entityTypeManager->getStorage('paragraph')->load($values['class_topic'])->get('field_class_topic')->getValue()[0]['value'],
      'field_class_timeslot' => $values['class_timeslot'],
    ]);
    $node->save();

    drupal_set_message($this->t('Registry created successfully.'));

    $form_state->setRebuild();
  }
}
