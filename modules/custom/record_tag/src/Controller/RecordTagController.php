<?php

namespace Drupal\record_tag\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal;

/**
 * Class RecordTagController.
 */
class RecordTagController extends ControllerBase {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ResourceController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * RecordTag.
   *
   * Return recordTag.
   */
  public function recordTag() {
      $form = $this->formBuilder()->getForm('Drupal\record_tag\Form\RecordTagForm');

      $query = Drupal::entityQuery('node')
            ->condition('type', 'tag')
            ->execute();

      if (!empty($query)) {
        foreach ($query as $row) {
            $data[] = $this->entityTypeManager->getStorage('node')->load($row);
        }
      }
      
      return [
          '#theme' => 'record_tag',
          '#title' => $this->t('Form Record TAG'),
          '#description' => $this->t('TAG Programming Assessment List'),
          '#form' => $form,
          '#tags' => $data 
      ];
  }

}